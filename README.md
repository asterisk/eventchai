# EventChai
Events and activities that are actually interesting to you and/or your family ought not be such a chore to hunt down. Eventchai spices up this challenge by learning about what your interested in and making smarter suggestions.

Furthermore, as an isolated platform, EventChai will do more to guard and protect your privacy from social media giants (like Facebook); by acting as a proxy – and anonymizing your interests & preferences – the sites we cull events and activities from will never have an opportunity to identify you personally.
